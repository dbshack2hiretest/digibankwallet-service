package main

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"io/ioutil"
	"math"
	"net/http"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/feature/rds/auth"
)


func main() {
	router := mux.NewRouter()
	router.HandleFunc("/hotels", listOfHotels).Methods("POST")
	router.HandleFunc("/hotel/signup", signUpHotel).Methods("POST")
	router.HandleFunc("/hotel/{hotelId}/coupons", getCoupons).Methods("GET")
	http.ListenAndServe(":8080", router)
}

type hotel struct {
	Name string `json:"Name"`
	Latitude float64 `json:"Latitude"`
	Longitude float64 `json:"Longitude"`
}

type hotelRegistration struct {
	Id string `json:"Id"`
	Name string `json:"Name"`
	Latitude float64 `json:"Latitude"`
	Longitude float64 `json:"Longitude"`
}

type location struct {
	Latitutde float64 `json:"lat"`
	Longitude float64 `json:"long"`
}

type hotelResponse struct {
	Name []string `json:"Name"`
}



func listOfHotels(w http.ResponseWriter,  r *http.Request){
	var response hotelResponse
	loc := unMarshalLocationRequestBody(r)

	err, db := callDb()
	defer db.Close()
	if err != nil {
		fmt.Println("Connection to sql error ", err)
	}
	var hotels []hotel

	rows, err2 := db.Query("SELECT * from hotel")
	if err2 != nil {
		fmt.Println("Select query error in hotel ", err2)
	}

	for rows.Next() {
		var h hotel
		err = rows.Scan(&h.Name, &h.Latitude, &h.Longitude)
		hotels = append(hotels, h)
	}
	for _, hotel := range hotels {
		if getDistanceFromLatLonInKm(hotel.Latitude, hotel.Longitude, loc.Latitutde, loc.Longitude) > 1 {
			response.Name = append(response.Name, hotel.Name)
		}
	}

	json.NewEncoder(w).Encode(response)
	w.WriteHeader(http.StatusOK)
}

func callDb() (error, *sql.DB) {
	dbName := "wallet"
	dbUser := "admin"
	dbHost := "dwallet.cs44sqm0uuz5.ap-southeast-1.rds.amazonaws.com"
	dbPort := 3306
	dbEndpoint := fmt.Sprintf("%s:%d", dbHost, dbPort)
	region := "ap-southeast-1a"

	cfg, err := config.LoadDefaultConfig(context.TODO())
	if err != nil {
		fmt.Println("configuration error: " + err.Error())
	}

	authenticationToken, err := auth.BuildAuthToken(
		context.TODO(), dbEndpoint, region, dbUser, cfg.Credentials)
	if err != nil {
		fmt.Println("failed to create authentication token: " + err.Error())
	}

	dsn := fmt.Sprintf("%s:%s@tcp(%s)/%s?tls=true&allowCleartextPasswords=true",
		dbUser, authenticationToken, dbEndpoint, dbName,
	)

	db, err := sql.Open("mysql", dsn)
	if err != nil {
		fmt.Println(err)
	}
	return err, db
}

func unMarshalHotelRegistrationRequestBody(request *http.Request) hotelRegistration {
	reqBody, _ := ioutil.ReadAll(request.Body)
	var hRegistration hotelRegistration
	json.Unmarshal(reqBody, &hRegistration)
	return hRegistration
}


func signUpHotel(w http.ResponseWriter,  r *http.Request){
	var response hotelResponse
	hRegistration := unMarshalHotelRegistrationRequestBody(r)

	err, db := callDb()
	defer db.Close()
	if err != nil {
		fmt.Println("Connection to sql error ", err)
	}
	createHotel(db, w, hRegistration)
	json.NewEncoder(w).Encode(response)
	w.WriteHeader(http.StatusCreated)
}

func unMarshalLocationRequestBody(request *http.Request) location {
	reqBody, _ := ioutil.ReadAll(request.Body)
	var loc location
	json.Unmarshal(reqBody, &loc)
	return loc
}


func getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2 float64) float64{
	var R = 6371 // Radius of the earth in km
	var dLat = deg2rad(lat2-lat1)  // deg2rad below
	var dLon = deg2rad(lon2-lon1)
	var a = math.Sin(dLat/2) * math.Sin(dLat/2) +
		math.Cos(deg2rad(lat1)) * math.Cos(deg2rad(lat2)) *
			math.Sin(dLon/2) * math.Sin(dLon/2)

	var c = 2 * math.Atan2(math.Sqrt(a), math.Sqrt(1-a))
	var d = float64(R) * c; // Distance in km
	return d
}

func deg2rad(deg float64) float64{
	return deg * (math.Pi/180)
}

func createHotel(db *sql.DB, w http.ResponseWriter, request hotelRegistration) {
	dbhotelRequest :=  hotelRegistration{
		Id: request.Id,
		Name: request.Name,
		Latitude: request.Latitude,
		Longitude: request.Longitude,
	}
	fmt.Println("dbhotelRequest", dbhotelRequest)
	stmt, err := db.Prepare("INSERT INTO hotel VALUES(?,?,?,?)")
	if err != nil {
	fmt.Println(err)
	}
	_, err = stmt.Exec(request.Name, request.Latitude, request.Longitude,request.Id)
	if err != nil {
		fmt.Println("Error in statament")
		fmt.Println(err)
	}
	fmt.Println(w, "New hotel registrered")
}

type hotelCouponsResponse struct {
	hotelId string `json:"HotelId"`
	coupons []string `json:"Coupons"`
}
type hotelCoupon struct {
	hotelId string `json:"hotelId"`
	coupons string `json:"coupons"`
}

func getCoupons(w http.ResponseWriter,  r *http.Request){
	vars := mux.Vars(r)
	hotelID, ok := vars["hotelId"]
	if !ok {
		fmt.Println("hotelID is missing in parameters")
	}


	err, db := callDb()
	defer db.Close()
	if err != nil {
		fmt.Println("Connection to sql error ", err)
	}




	var cs []string

	rows, err2 := db.Query("SELECT * from hotel_coupons where hotelID = ?", hotelID)
	if err2 != nil {
		fmt.Println("Select query error in hotel ", err2)
	}

	for rows.Next() {
		var h hotelCoupon
		err = rows.Scan(&h.hotelId, &h.coupons)
		cs = append(cs,h.coupons)
	}

	response2 := hotelCouponsResponse{
		hotelId: hotelID,
		coupons: cs,
	}
	fmt.Println("response2------ ", response2)
	json.NewEncoder(w).Encode(response2)
}